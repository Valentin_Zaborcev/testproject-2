#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"

class GameScene : public cocos2d::LayerGradient {

public:
	cocos2d::Sprite *mySprite;

    static cocos2d::Scene* createScene();

    virtual bool init();
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);

    // implement the "static create()" method manually
    CREATE_FUNC(GameScene);
	bool onTapBegan(cocos2d::Touch* touch, cocos2d::Event* event);
	void onTapEnded(cocos2d::Touch* touch, cocos2d::Event* event);

};

#endif // __HELLOWORLD_SCENE_H__
