#include <vector>
#include "MenuScene.h"
#include "GameScene.h"
#include "GameOverScene.h"
#include "SimpleAudioEngine.h"
#include "../gameClasses/Cloud.h"
#include "../gameClasses/CloudGenerator.h"
#include "configurator/Configurator.h"
#include "../gameClasses/Bird.h"
#include "ui/CocosGUI.h"
#include "ScoreScene.h"

USING_NS_CC;

Scene* GameOverScene::createScene(){
    auto scene = Scene::create();

    auto layer = GameOverScene::create();

    scene->addChild(layer);

    return scene;
}

bool GameOverScene::init(){
    if ( !LayerGradient::initWithColor(Color4B(155, 238, 255, 255), Color4B(63, 184, 255, 255)) ){
        return false;
    }

	this->addChild(createMenu());
	if (config::Configurator::getInstance()->gotInTable("game/highscores/records.txt")) { 
		addWinnerLabels();
	}
	else {
		addLoserLabels();
	}
    return true;
}

cocos2d::Menu* GameOverScene::createMenu() {
	TTFConfig config;
	config.outlineSize = 1;
	config.fontFilePath = "fonts/komika.ttf";
	config.fontSize = 26;

	auto menuItems = Vector<MenuItem*>();
	auto label = Label::createWithTTF(config, "Play Again");
	menuItems.pushBack(MenuItemLabel::create(label, CC_CALLBACK_1(GameOverScene::playAgain, this)));
	label = Label::createWithTTF(config, "HighScores");
	menuItems.pushBack(MenuItemLabel::create(label, CC_CALLBACK_1(GameOverScene::highScores, this)));
	label = Label::createWithTTF(config, "Main Menu");
	menuItems.pushBack(MenuItemLabel::create(label, CC_CALLBACK_1(GameOverScene::mainMenu, this)));
	
	auto menu = cocos2d::Menu::createWithArray(menuItems);
	menu->alignItemsVerticallyWithPadding(15);
	menu->setColor(Color3B(255, 230, 183));
	menu->setPositionY(Director::getInstance()->getVisibleSize().height / 4);
	return menu;	
}

void GameOverScene::addWinnerLabels() {
	TTFConfig config;
	config.outlineSize = 1;
	config.fontFilePath = "fonts/komika.ttf";
	config.fontSize = 40;
	auto label = Label::createWithTTF(config, "CONGRATULATIONS!");
	label->setVerticalAlignment(TextVAlignment::TOP);
	label->setHorizontalAlignment(TextHAlignment::CENTER);
	label->setPosition(Vec2(Director::getInstance()->getVisibleSize().width / 2,
		Director::getInstance()->getVisibleSize().height - config.fontSize));
	label->setColor(Color3B(255, 230, 183));
	this->addChild(label);

	char buf[255];
	config.fontSize = 30;
	sprintf(buf, "You've got %d points!", config::Configurator::getInstance()->getPoints());
	auto label2 = Label::createWithTTF(config, buf);
	label2->setColor(Color3B(255, 230, 183));
	label2->setPosition(Vec2(Director::getInstance()->getVisibleSize().width / 2,
		Director::getInstance()->getVisibleSize().height / 4 * 3));
	this->addChild(label2);

	auto txtfield = cocos2d::ui::TextField::create("Enter your name", "fonts/komika.ttf", 20);
	txtfield->setPosition(Vec2(Director::getInstance()->getVisibleSize().width / 2,
		Director::getInstance()->getVisibleSize().height / 5 * 3));
	txtfield->setColor(Color3B(255, 230, 183));
	txtfield->setMaxLength(12);
	txtfield->setMaxLengthEnabled(true);
	this->addChild(txtfield);

	auto button = cocos2d::ui::Button::create();
	button->setPosition(Vec2(txtfield->getPosition().x + txtfield->getPosition().x / 2,
		txtfield->getPosition().y));
	button->setTitleText("Save results!");
	button->setTitleFontSize(18);
	button->setTitleFontName("fonts/komika.ttf");
	this->addChild(button);

	button->addTouchEventListener([&](Ref* pSender, ui::Widget::TouchEventType type) {
		if (type == ui::Widget::TouchEventType::ENDED)	{
			saveResults("game/highscores/records.txt", txtfield->getString().c_str());
			auto scene = ScoreScene::createScene();
			Director::getInstance()->replaceScene(TransitionFade::create(0.3, scene));
		}
	});
}

void GameOverScene::addLoserLabels() {
	TTFConfig config;
	config.outlineSize = 1;
	config.fontFilePath = "fonts/komika.ttf";
	config.fontSize = 40;
	auto label = Label::createWithTTF(config, "GAME OVER!");
	label->setVerticalAlignment(TextVAlignment::TOP);
	label->setHorizontalAlignment(TextHAlignment::CENTER);
	label->setPosition(Vec2(Director::getInstance()->getVisibleSize().width / 2,
		Director::getInstance()->getVisibleSize().height - config.fontSize));
	label->setColor(Color3B(255, 230, 183));
	this->addChild(label);

	char buf[255];
	config.fontSize = 30;
	sprintf(buf, "You've got only %d points!", config::Configurator::getInstance()->getPoints());
	auto label2 = Label::createWithTTF(config, buf);
	label2->setColor(Color3B(255, 230, 183));
	label2->setPosition(Vec2(Director::getInstance()->getVisibleSize().width / 2,
		Director::getInstance()->getVisibleSize().height / 4 * 3));
	this->addChild(label2);	
}

void GameOverScene::saveResults(const string& fName, const char* person) {
	std::vector<std::pair<std::string, int>> table;

	FILE* fp = std::fopen(fName.c_str(), "r");
	if (!fp) return;
	char buf[255] = "";
	for (int i = 0; i < 10; i++) {
		int a;
		std::fscanf(fp, "%s", &buf);
		std::fscanf(fp, "%d", &a);
		table.push_back(std::make_pair(buf, a));
	}
	table.push_back(std::pair<const char*, int>(person, config::Configurator::getInstance()->getPoints()));
	std::sort(table.begin(), table.end(), [](auto &left, auto &right) {
		return left.second > right.second;
	});
	table.erase(table.end() - 1);

	fclose(fp);
	fp = std::fopen(fName.c_str(), "w");
	for (auto i = table.begin(); i != table.end(); i++) {
		std::fprintf(fp, "%s %d\n", i->first.c_str(), i->second);
	}
	fclose(fp);	
}

void GameOverScene::playAgain(cocos2d::Ref* pSender) {
	auto scene = GameScene::createScene();
	Director::getInstance()->replaceScene(TransitionFade::create(0.3, scene));
}

void GameOverScene::mainMenu(cocos2d::Ref* pSender) {
	auto scene = MenuScene::createScene();
	Director::getInstance()->replaceScene(TransitionFade::create(0.3, scene));
}

void GameOverScene::highScores(cocos2d::Ref* pSender) {
	auto scene = ScoreScene::createScene();
	Director::getInstance()->replaceScene(TransitionFade::create(0.3, scene));
}
//
void GameOverScene::menuCloseCallback(Ref* pSender)
{
    Director::getInstance()->end();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}
