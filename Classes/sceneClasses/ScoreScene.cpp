#include <vector>
#include "MenuScene.h"
#include "ScoreScene.h"
#include "GameScene.h"
#include "SimpleAudioEngine.h"
#include "../gameClasses/Cloud.h"
#include "../gameClasses/CloudGenerator.h"
#include "configurator/Configurator.h"
#include "../gameClasses/Bird.h"
#include "GameOverScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

Scene* ScoreScene::createScene() {
	auto scene = Scene::create();

	auto layer = ScoreScene::create();

	scene->addChild(layer);

	return scene;
}


bool ScoreScene::init() {
	if (!LayerGradient::initWithColor(Color4B(155, 238, 255, 255), Color4B(63, 184, 255, 255))) {
		return false;
	}

	this->addChild(createMenu());
	auto scroll = ui::ScrollView::create();
	//scroll->setDirection(ui::ScrollView::Direction::VERTICAL);
	scroll->setContentSize(Size(300, 600));
	scroll->setInnerContainerSize(Size(300, 400));
	scroll->setBounceEnabled(true);
	scroll->setAnchorPoint(Vec2(0.5, 0.5));
	scroll->setPosition(Vec2(Director::getInstance()->getVisibleSize().width * 3 / 5, 
		Director::getInstance()->getVisibleSize().height * 4 / 5));
	TTFConfig config;
	config.outlineSize = 1;
	config.fontFilePath = "fonts/komika.ttf";
	config.fontSize = 26;

	FILE* fp =	std::fopen("game/highscores/records.txt", "r");
	std::vector<std::pair<std::string, int>> table;
	char buf[255] = "";
	int j = 0;
	for (int i = 0; i < 10; i++) {
		int a;
		std::fscanf(fp, "%s", &buf);
		CCLOG("%s", buf);
		std::fscanf(fp, "%d", &a);
		table.push_back(std::make_pair(buf, a));
	}
	for (auto i = table.rbegin(); i != table.rend(); i++) {
		sprintf(buf, "%s %d", i->first.c_str(), i->second);
		auto label = Label::createWithTTF(config, buf);
		label->setPosition(Vec2(scroll->getContentSize().width / 2, j * 32));
		scroll->addChild(label);
		j++;
	}
	fclose(fp);	
	this->addChild(scroll);

	return true;
}

cocos2d::Menu* ScoreScene::createMenu() {
	TTFConfig config;
	config.outlineSize = 1;
	config.fontFilePath = "fonts/komika.ttf";
	config.fontSize = 26;

	auto menuItems = Vector<MenuItem*>();
	auto label = Label::createWithTTF(config, "Play Game");
	menuItems.pushBack(MenuItemLabel::create(label, CC_CALLBACK_1(ScoreScene::PlayGame, this)));
	label = Label::createWithTTF(config, "Back To Menu");
	menuItems.pushBack(MenuItemLabel::create(label, CC_CALLBACK_1(ScoreScene::BackToMenu, this)));

	auto menu = cocos2d::Menu::createWithArray(menuItems);
	menu->alignItemsVerticallyWithPadding(15);
	menu->setColor(Color3B(255, 230, 183));
	menu->setPositionY(Director::getInstance()->getVisibleSize().height * 2 / 5);
	menu->setPositionX(Director::getInstance()->getVisibleSize().width * 1 / 5);
	return menu;
}

void ScoreScene::PlayGame(cocos2d::Ref* pSender) {
	auto scene = GameScene::createScene();
	Director::getInstance()->replaceScene(TransitionFade::create(0.3, scene));
}

void ScoreScene::BackToMenu(cocos2d::Ref* pSender) {
	auto scene = MenuScene::createScene();
	Director::getInstance()->replaceScene(TransitionFade::create(0.3, scene));
}

void ScoreScene::menuCloseCallback(Ref* pSender)
{
	Director::getInstance()->end();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif
}