#include <vector>
#include "MenuScene.h"
#include "GameScene.h"
#include "SimpleAudioEngine.h"
#include "../gameClasses/Cloud.h"
#include "../gameClasses/CloudGenerator.h"
#include "configurator/Configurator.h"
#include "../gameClasses/Bird.h"
#include "GameOverScene.h"
#include "ScoreScene.h"

USING_NS_CC;

Scene* MenuScene::createScene(){
    auto scene = Scene::create();

    auto layer = MenuScene::create();

    scene->addChild(layer);

    return scene;
}

void MenuScene::addEvent() {
	auto _listener = cocos2d::EventListenerTouchOneByOne::create();
	_listener->setSwallowTouches(true);

	_listener->onTouchBegan = [&](cocos2d::Touch* touch, cocos2d::Event* event) {
		cocos2d::Vec2 p = touch->getLocation();
		cocos2d::Rect rect = this->getBoundingBox();
		auto parent = this->getParent();
		if (rect.containsPoint(p)) {
			return true;
		}
		return false;
	};

	_listener->onTouchEnded = [&](cocos2d::Touch* touch, cocos2d::Event* event) {
		
	};

	_listener->onTouchMoved = [&](cocos2d::Touch* touch, cocos2d::Event* event) {
		CCLOG("%f %f", touch->getLocation().x, touch->getLocation().y);
	};

	cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(_listener, this);
}

bool MenuScene::init(){
    if ( !LayerGradient::initWithColor(Color4B(155, 238, 255, 255), Color4B(63, 184, 255, 255)) ){
        return false;
    }
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto cloudGenerator = CloudGenerator::create();
	cloudGenerator->generateWave(7);

	addEvent();

	this->addChild(cloudGenerator);
	this->addChild(createMenu());

    return true;
}

cocos2d::Menu* MenuScene::createMenu() {	
	TTFConfig config;
	config.outlineSize = 1;
	config.fontFilePath = "fonts/komika.ttf";
	config.fontSize = 26;

	auto menuItems = Vector<MenuItem*>();
	auto label = Label::createWithTTF(config, "Play Game");
	menuItems.pushBack(MenuItemLabel::create(label, CC_CALLBACK_1(MenuScene::PlayGame, this)));
	label = Label::createWithTTF(config, "HighScores");
	menuItems.pushBack(MenuItemLabel::create(label, CC_CALLBACK_1(MenuScene::HighScores, this)));
	label = Label::createWithTTF(config, "Exit");
	menuItems.pushBack(MenuItemLabel::create(label, CC_CALLBACK_1(MenuScene::menuCloseCallback, this)));
	
	auto menu = cocos2d::Menu::createWithArray(menuItems);
	menu->alignItemsVerticallyWithPadding(15);
	menu->setColor(Color3B(255, 230, 183));
	menu->setPositionY(Director::getInstance()->getVisibleSize().height * 2 / 5);
	return menu;	
}

void MenuScene::PlayGame(cocos2d::Ref* pSender) {
	auto scene = GameScene::createScene();
	Director::getInstance()->replaceScene(TransitionFade::create(0.3, scene));
}

void MenuScene::HighScores(cocos2d::Ref* pSender) {
	auto scene = ScoreScene::createScene();
	Director::getInstance()->replaceScene(TransitionFade::create(0.3, scene));
}

void MenuScene::menuCloseCallback(Ref* pSender)
{
    Director::getInstance()->end();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}
