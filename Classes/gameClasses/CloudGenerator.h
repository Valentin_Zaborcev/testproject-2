#ifndef __CLOUD_GENERATOR_H__
#define __CLOUD_GENERATOR_H__

#include "cocos2d.h"
#include "Cloud.h"

class CloudGenerator : public cocos2d::Layer {
protected:
	CloudGenerator() {}
	CloudGenerator(const CloudGenerator&) {}

public:
	virtual ~CloudGenerator() {}
	static CloudGenerator* create();
	void generateWave(int cloudAmount);

private:
	cocos2d::Vector<Cloud*> _clouds;
};


#endif // __CLOUD_GENERATOR_H__