#include "cocos2d.h"
#include "BirdWaveGenerator.h"
#include "../configurator/Configurator.h"

#define RANDD(a, b) cocos2d::RandomHelper::random_real(a, b);
#define RANDI(a, b) cocos2d::RandomHelper::random_int(a, b);

using namespace cocos2d;

BirdWaveGenerator* BirdWaveGenerator::create() {
	auto birdWaveGenerator = new BirdWaveGenerator();
	if (birdWaveGenerator->init()) {
		birdWaveGenerator->autorelease();
		return birdWaveGenerator;
	}
	return nullptr;
}

void BirdWaveGenerator::gameLoop() {
	auto generateWave = CallFunc::create([&]() {
		auto vSize = Director::getInstance()->getVisibleSize();
		auto avgAmount = RANDI(-1, 1);
		for (int i = 0; i < _birdsAvgAmount + avgAmount; i++) {

			auto bird = Bird::create(Vec2::ZERO);
			auto posY = RANDD(0.f + config::Configurator::getInstance()->birdAverageHeight,
				vSize.height - config::Configurator::getInstance()->birdAverageHeight);
			bird->setPosition(Vec2(vSize.width + bird->getContentSize().width / 2, posY));
			float speed = RANDI(100 + 5 * _birdsAvgAmount, 130 + 6 * _birdsAvgAmount);
			bird->setSpeed(speed);
			bird->startMove();
			addChild(bird, _z);
			_z--;
		}

		if (_birdsAvgAmount < _birdsMaxAmount)
			_birdsAvgAmount++;
	});

	auto action = Sequence::create(generateWave, DelayTime::create(_waveTimer), nullptr);
	_currentAction = RepeatForever::create(action);
	this->runAction(_currentAction);
}