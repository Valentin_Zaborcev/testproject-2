#include "Bird.h"
#include "cocos2d.h"
#include "../sceneClasses/GameOverScene.h"

using namespace cocos2d;

Bird::~Bird() {
	removeListener();
}

Bird* Bird::create(const cocos2d::Vec2& pos) {
	Bird* bird = new Bird();
	char str[100] = { 0 };
	auto birdType = rand() % 4;
	sprintf(str, "game/bird%d/1.png", birdType);
	if (bird->initWithFile(str)) {
		bird->autorelease();
		bird->setBirdType(birdType);
		bird->_addEventListener();
		bird->setPosition(pos);
		return bird;
	}
	return nullptr;
}

void Bird::removeListener() {
	if (_listener != nullptr) {
		cocos2d::Director::getInstance()->getEventDispatcher()->removeEventListener(_listener);
		_listener = nullptr;
	}
}

void Bird::_addEventListener() {
	_listener = cocos2d::EventListenerTouchOneByOne::create();
	_listener->setSwallowTouches(true);

	_listener->onTouchBegan = [&](cocos2d::Touch* touch, cocos2d::Event* event) {
		cocos2d::Vec2 p = touch->getLocation();
		cocos2d::Rect rect = this->getBoundingBox();
		auto parent = this->getParent();
		if (rect.containsPoint(p)) {
			return true;
		}
		return false;
	};

	_listener->onTouchEnded = [&](cocos2d::Touch* touch, cocos2d::Event* event) {
		stopAction(_currentAction);
		stopAction(_currentAnimation);
		removeListener();

		char str[100];
		sprintf(str, "game/bird%d/5.png", _birdType);
		this->initWithFile(str);

		auto spawn = Spawn::create(JumpBy::create(0.15, Vec2(30, 0), 20, 1),
			RotateBy::create(0.25, 80), nullptr);
		auto _currentAction = Sequence::create(spawn, MoveTo::create(0.3, Vec2(this->getPosition().x, - this->getContentSize().width / 2)),
			RemoveSelf::create(), nullptr);
		this->runAction(_currentAction);
		config::Configurator::getInstance()->addPoints(10);
		config::Configurator::getInstance()->label->setString(std::to_string(config::Configurator::getInstance()->getPoints()));
	};
	cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(_listener, 30);
}

void Bird::startMove() {
	//Animation
	auto animation = Bird::_createAnimation("game/bird%d/%d.png", _birdType, 4);
	auto animate = Animate::create(animation);	_currentAnimation = RepeatForever::create(animate);	this->runAction(_currentAnimation);

	//Moving
	auto actualCoords = this->getPosition();
	auto distance = Director::getInstance()->getVisibleSize().width;
	auto duration = distance / RandomHelper::random_int(100, 150);
	auto moveAction = EaseInOut::create(MoveTo::create(duration, Vec2(-this->getContentSize().width / 2, actualCoords.y)), RandomHelper::random_real(1.5f, 2.f));
	_currentAction = Sequence::create(moveAction, CallFunc::create([&]() {
		auto scene = GameOverScene::createScene();
		Director::getInstance()->replaceScene(cocos2d::TransitionFade::create(0.3, scene));
	}), nullptr);
	this->runAction(_currentAction);
}