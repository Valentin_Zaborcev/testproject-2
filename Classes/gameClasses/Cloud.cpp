#include "Cloud.h"

using namespace cocos2d;

Cloud* Cloud::create(const cocos2d::Vec2& pos) {
	auto cloud = new Cloud();
	char str[100] = { 0 };
	sprintf(str, "game/clouds/cloud%d.png", rand() % 3);
	if (cloud->initWithFile(str)) {
		cloud->autorelease();
		cloud->setPosition(pos);
		return cloud;
	}
	return nullptr;
}

Cloud::~Cloud() {

}

void Cloud::startMove() {
	if (_currentAction != nullptr) {
		stopAction(_currentAction);
	}
	auto pos = getPosition();
	auto distance = Director::getInstance()->getVisibleSize().width;
	auto time = distance / _speed;
	auto finalPositionX = - this->getContentSize().width / 2;
	auto moveTo = MoveTo::create(time, Vec2(finalPositionX, this->getPosition().y));
	auto newPosition = MoveTo::create(0, Vec2(distance + this->getContentSize().width / 2, this->getPosition().y));
	_currentAction = RepeatForever::create(Sequence::create(moveTo, newPosition, nullptr));
	runAction(_currentAction);
}

void Cloud::removeItSelf() {
	this->removeFromParent();
}