#include "cocos2d.h"
#include "SimpleObject.h"
#include "configurator\Configurator.h"

using namespace cocos2d;

Animation* SimpleObject::_createAnimation(const std::string& model, const int& type,
	const int& frames, float animationTime) {
	Vector<SpriteFrame*> animFrames(frames);
	char str[100] = { 0 };
	sprintf(str, model.c_str(), type, 1);
	auto sprite = Sprite::create(str);

	for (int i = 1; i <= frames; i++) {
		sprintf(str, model.c_str(), type, i);
		auto frame = SpriteFrame::create(str, Rect(0, 0, sprite->getContentSize().width, sprite->getContentSize().height));
		animFrames.pushBack(frame);
	}
	auto animation = Animation::createWithSpriteFrames(animFrames, animationTime);
	return animation;
}