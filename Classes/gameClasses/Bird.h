#ifndef __BIRD_H_
#define __BIRD_H_

#include "cocos2d.h"
#include "SimpleObject.h"

class Bird : public SimpleObject {
public:
	static Bird* create(const cocos2d::Vec2& pos);
	void removeListener();
	void startMove() override;
	void setBirdType(int birdType) { _birdType = birdType; };
	int getBirdType() const { return _birdType; };

protected:
	Bird() {}
	~Bird();
	void _addEventListener();

private:
	cocos2d::EventListenerTouchOneByOne*  _listener = nullptr;
	int _birdType;
};

#endif // __BIRD_H_